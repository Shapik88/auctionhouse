CREATE TABLE `auctionhouse`.`buyers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(60) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `phone_number` INT(20) NULL,
  `buyer's_auction_number` INT NOT NULL,
  `buyer's_address` VARCHAR(80) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


CREATE TABLE `auctionhouse`.`product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `product_name` VARCHAR(45) NOT NULL,
  `product_price` INT(9) NOT NULL,
  `product_category` VARCHAR(45) NOT NULL,
  `years_product` INT NOT NULL,
  `product_auction_number` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `auctionhouse`.`seler` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `product_auction_number` INT NOT NULL,
  `Bank account number` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
